import 'package:flutter/material.dart';

import 'package:chatmgw/pages/chat_page.dart';
import 'package:chatmgw/pages/loading_page.dart';
import 'package:chatmgw/pages/login_page.dart';
import 'package:chatmgw/pages/register_page.dart';
import 'package:chatmgw/pages/usuarios_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'usuarios': (_) => UsuariosPage(),
  'chat': (_) => ChatPage(),
  'login': (_) => LoginPage(),
  'register': (_) => RegisterPage(),
  'loading': (_) => LoadingPage(),
};
